var map = L.map('map').setView({ lat: 46.7, lng: 7.05 }, 9);

L.tileLayer('https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png', {
  maxZoom: 19,
  attribution: '&copy; <a href="https://openstreetmap.org/copyright">OpenStreetMap contributors</a>'
}).addTo(map);

L.control.scale().addTo(map);


// La feature collection avec les géométries éditables.
var fcoll = null;

// Obtenir les géométries existantes
fetch('/geometries')
  .then(function(response){ return response.json(); })
  .then(function(data){
    
    // Charger les géométries
    fcoll = L.geoJSON(data);

    // Ajouter les géométries à la carte
    map.addLayer(fcoll);

    // La boîte à outils pour éditer les géométries
    var drawControl = new L.Control.Draw({
      draw: {
        marker: false,
      },
      edit: {
        featureGroup: fcoll,
      }
    });
    map.addControl(drawControl);

  });


// Les fonctions pour sauvegarder les géométries modifiées.
map.on(L.Draw.Event.CREATED, function(e){
  fcoll.addLayer(e.layer);
  save_geometries()
});
map.on(L.Draw.Event.EDITED, function(e){ save_geometries(); });
map.on(L.Draw.Event.DELETED, function(e){ save_geometries(); });


// Envoyer les géométries à l'application Flask qui va pouvoir enregistrer les
// données dans PostGIS.
function save_geometries(){
  postData('/save_geometries', fcoll.toGeoJSON())
    .then(function(response){
      console.log(response);
    });
}


async function postData(url = '', data = {}) {
  const response = await fetch(url, {
    method: 'POST',
    mode: 'cors',
    cache: 'no-cache',
    credentials: 'same-origin',
    headers: { 'Content-Type': 'application/json' },
    redirect: 'follow',
    referrerPolicy: 'no-referrer',
    body: JSON.stringify(data)
  });
  return response.json();
}
