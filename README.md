# flask-leaflet-draw

Exemple d'une application Flask qui montre comment utiliser le plugin Leaflet-Draw pour éditer des géométries.

Les géométries sont envoyées au serveur Flask qui les enregistre dans une base de données PostGIS. La structure de la base de données se trouve dans `leaflet_draw.sql`.

Bien évidemment, dans une application réelle il faudrait protéger l'application Flask avec une logique d'authentification et autorisation.
