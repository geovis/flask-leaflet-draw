from flask import Flask, jsonify, render_template, request
import json
import psycopg2 as psql


app = Flask(__name__)
app.debug = True

db = psql.connect(dbname='leaflet_draw', host='127.0.0.1')


@app.route('/')
def index():
    return render_template('index.html')



@app.route('/geometries')
def get_geometries():
    cur = db.cursor()
    cur.execute("""SELECT ST_AsGeoJson(geom) FROM feats""")
    feat_coll = {"type": "FeatureCollection", "features": []}
    feats = cur.fetchall()
    for feat in feats:
        feat_coll['features'].append({
            "type": "Feature",
            "properties": {},
            "geometry": json.loads(feat[0])
        })
    return jsonify(feat_coll)



@app.route('/save_geometries', methods=['POST'])
def save_geometries():
    feat_coll = json.loads(request.data.decode('utf8'))
    cur = db.cursor()
    # Supprimer toute la table pour insérer à nouveau l'ensemble des
    # géométries. Pas très efficace mais pour la démo ça fait l'affaire.
    cur.execute('DELETE FROM feats')
    for feat in feat_coll['features']:
        cur.execute(
            """INSERT INTO feats (geom) VALUES (ST_GeomFromGeoJSON(%s))""",
            (json.dumps(feat['geometry']), )
        )

    cur.close()
    db.commit()
    return {'message': 'success'}



if __name__ == '__main__':
    app.debug = True
    app.run()
